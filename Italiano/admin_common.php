<?php
$lang_admin_common = array(
 'Admin menu' => 'Menu amministratore',
 'Plugins menu' => 'Menu plugin',
 'Moderator menu' => 'Menu moderatore',
 'Index' => 'Indice',
 'Categories' => 'Categorie',
 'Forums' => 'Forum',
 'Users' => 'Utenti',
 'User groups' => 'Gruppi utente',
 'Options' => 'Opzioni',
 'Permissions' => 'Permessi',
 'Censoring' => 'Oscuramenti',
 'Bans' => 'Interdizioni',
 'Prune' => 'Pulizia',
 'Maintenance' => 'Manutenzione',
 'Reports' => 'Segnalazioni',
 'Server statistics' => 'Statistiche server',
 'Admin' => 'Admin',
 'Go back' => 'Indietro',
 'Delete' => 'Cancella',
 'Update' => 'Aggiorna',
 'Add' => 'Aggiungi',
 'Edit' => 'Modifica',
 'Remove' => 'Cancella',
 'Yes' => 'S&igrave;',
 'No' => 'No',
 'Save changes' => 'Salva modifiche',
 'Save' => 'Salva',
 'here' => 'qui',
 'Action' => 'Azione',
 'None' => 'Niente',
 'Maintenance mode' => 'modo manutenzione',
 'No plugin message' => 'Nella cartella dei plugin non &egrave; presente alcun plugin denominato %s.',
 'Plugin failed message' => 'Caricamento del plugin - <strong>%s</strong> - non riuscito.'
);
?>
