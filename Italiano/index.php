<?php
$lang_index = array(
 'Topics' => 'Discussioni',
 'Link to' => 'Collegamento a:',
 'Empty board' => 'Forum vuoto.',
 'Newest user' => 'Benvenuto a: %s !',
 'Users online' => 'Utenti in linea: %s',
 'Guests online' => 'Ospiti in linea: %s',
 'No of users' => 'Utenti totali: %s',
 'No of topics' => 'Discussioni totali: %s',
 'No of posts' => 'Messaggi totali: %s',
 'Online' => 'In linea:',
 'Board info' => 'Informazioni forum',
 'Board stats' => 'Statistiche forum',
 'User info' => 'Informazioni utente'
);
?>
