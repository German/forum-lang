<?php
$lang_admin_reports = array(
 'Report zapped redirect' => 'Отчёт прочитан, перенаправление ...',
 'New reports head' => 'Новые отчёты',
 'Deleted user' => 'Удалённый пользователь',
 'Deleted' => 'Удалён',
 'Post ID' => 'Пост #%s',
 'Report subhead' => 'Сообщено %s',
 'Reported by' => 'Сообщил %s',
 'Reason' => 'Ответ',
 'Zap' => 'Прочитано',
 'No new reports' => 'Нет новых отчётов',
 'Last 10 head' => '10 последних прочитанных отчётов',
 'NA' => 'Не определено',
 'Zapped subhead' => 'Прочитано %s было %s',
 'No zapped reports' => 'Нет прочитанных отчётов.'
);
?>
