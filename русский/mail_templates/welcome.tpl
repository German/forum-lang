Subject: Добро пожаловать на <board_title>!

Спасибо Вам за регистрацию на форуме <base_url>. Информаиця о Вашей учётной записи:

Имя пользователя: <username>
Пароль: <password>

Перейдите по <login_url> для активации вашей учётной записи.

--
<board_mailer> Отправитель
(Пожалуйста, не отвечайте на данное сообщение)
