<?php

// Language definitions used in register.php
$lang_register = array(

// Miscellaneous
 'No new regs' => 'Dieses Forum akzeptiert keine neuen Registrierungen.',
 'Reg cancel redirect' => 'Registrierung abgebrochen. Weiterleitung…',
 'Forum rules' => 'Forums-Regeln',
 'Rules legend' => 'Du musst den folgenden Richtlinien zustimmen, um registriert zu werden.',
 'Registration flood' => 'Innerhalb der letzten Stunde wurde ein neuer Benutzer mit der gleichen IP wie Du registriert. Um eine Flut von Registrierungen zu verhindern, muss mindestens eine Stunde zwischen zwei Registrierungen von derselben IP liegen. Sorry für die Unannehmlichkeiten.',
 'Agree' => 'Zustimmen',
 'Cancel' => 'Abbrechen',
 'Register' => 'Registrieren',

// Form validation stuff (some of these are also used in post.php)
 'Registration errors' => 'Fehler bei der Registrierung',
 'Registration errors info' => 'Die folgenden Fehler müssen behoben werden, bevor Du dich registrieren kannst:',
 'Username censor' => 'Der eingegebene Benutzername enthält ein oder mehrere zensierte Wörter. Bitte wähle einen anderen Benutzernamen.',
 'Username dupe 1' => 'Es gibt bereits eine Registrierung mit diesem Benutzernamen.',
 'Username dupe 2' => 'Der eingegebene Benutzername ist zu ähnlich. Der Benutzername muss mindestens in einem alphanumerischen Zeichen (a-z oder 0-9) davon abweichen. Bitte wähle einen anderen Benutzernamen.',
 'Email not match' => 'Die Email sind unterschiedlich.',

// Registration email stuff
 'Reg email' => 'Danke für die Registrierung. Dein Passwort wurde an die angegebene Email Adresse geschickt. Sollte die Email nicht ankommen, kontaktiere den Forums Administrator unter',
 'Reg complete' => 'Registrierung erfolgreich. Einloggen und Weiterleiten…',

// Register info
 'Desc 1' => 'Die Registrierung bietet Zugang zu Eigenschaften und Möglichkeiten, die ansonsten nicht zur Verfügung stehen. Das beinhaltet u.a. die Möglichkeit, Beiträge zu ändern und zu löschen, das Anlegen einer eigenen Signature, die in jedem deiner Beiträge erscheint und vieles mehr. Bei allen Fragen zu diesem Forum wende dich bitte an den Administrator.',
 'Desc 2' => 'Das untenstehende Formular muss ausgefüllt werden, damit Du registriert werden kannst. Im Anschluss an die Registrierung solltest Du in dein Profil wechseln und die Einträge und Einstellungen überprüfen. Die Felder hier machen nur einen kleinen Teil der Einstellungen in deinem Profil aus.',
 'Username legend' => 'Bitte gib einen Benutzernamen mit 2 bis 25 Zeichen ein',
 'Pass legend' => 'Bitte Passwort eingeben und bestätigen',
 'Pass info' => 'Passwörter müssen mindestens 6 Zeichen lang sein. Groß/Kleinschreibung wird berücksichtigt.',
 'Email info' => 'Du musst eine gültige Email Adresse eingeben, da das zufällig erzeugte Passwort an diese Adresse geschickt wird.',
 'Confirm email' => 'Email Addresse bestätigen',
 'If human' => 'Wenn Du ein Mensch bist, lasse dieses Feld leer!',
 'Spam catch' => 'Unglücklicherweise sieht es so aus, als wäre dein Antrag Spam. Sollte diese Einschätzung falsch sein, sende bitte eine Anfrage direkt an den Forums Administrator unter',

);
