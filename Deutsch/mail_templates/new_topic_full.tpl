Betreff: Neues Thema im Forum: '<forum_name>'

<poster> hat ein neues Thema '<topic_subject>' im Forum '<forum_name>', das Du abonniert hast, eröffnet.

Link zum neuen Thema <topic_url>

Der Beitrag lautet wie folgt:
-----------------------------------------------------------------------

<message>

-----------------------------------------------------------------------

Du kannst dein Abonnement beenden, indem Du auf <unsubscribe_url> den Link "Abonnement beenden" aufrufst.

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
