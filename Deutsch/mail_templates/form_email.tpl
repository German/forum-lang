Betreff: <mail_subject>

<sender> aus <board_title> hat dir eine Nachricht geschickt. Du kannst <sender> antworten, indem Du auf diese Email direkt antwortest.

Die Nachricht lautet wie folgt:
-----------------------------------------------------------------------

<mail_message>

-----------------------------------------------------------------------

--
<board_mailer> Mailer
