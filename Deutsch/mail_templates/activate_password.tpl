Betreff: Neues Passwort angefordert

Hallo <username>,

Für dein Konto im Diskussions-Forum <base_url> hast Du ein neues Passwort angefordert. Wenn diese Anforderung nicht von dir kommt oder wenn Du keine Passwort-Änderung mehr wünscht, ignoriere bitte diese Email. Dein Passwort wird erst geändert, wenn Du die untenstehende Aktivierungsseite aufrufst.

Dein neues Passwort ist: <new_password>

Um das Passwort zu ändern, rufe bitte die folgende Seite auf:
<activation_url>

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
