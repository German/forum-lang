<?php
$lang_delete = array(
 'Delete post' => 'Beitrag löschen',
 'Warning' => 'Du bist gerade dabei, diesen Beitrag unwiderruflich zu löschen.',
 'Topic warning' => 'Warnung! Das ist der erste Beitrag in diesem Thema, damit wird das gesamte Thema unwiderruflich gelöscht.',
 'Delete info' => 'Der Beitrag, den Du zum Löschen ausgewählt hast, wird dir weiter unten noch einmal zur Prüfung vorgelegt, bevor die Löschung ausgeführt wird.',
 'Reply by' => 'Antwort von %s - %s',
 'Topic by' => 'Thema begonnen von %s - %s',
 'Delete' => 'Löschen', // The submit button
 'Post del redirect' => 'Beitrag gelöscht. Weiterleitung …',
 'Topic del redirect' => 'Thema gelöscht. Weiterleitung …'
);
?>
