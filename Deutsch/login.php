<?php
$lang_login = array(
 'Login errors' => 'Login Fehler',
 'Login errors info' => 'Folgender Fehler muss korrigiert werden, bevor Du dich einloggen kannst:',
 'Wrong user/pass' => 'Falscher Benutzername und/oder falsches Passwort.',
 'Forgotten pass' => 'Passwort vergessen?',
 'Login redirect' => 'Erfolgreich eingeloggt. Weiterleitung …',
 'Logout redirect' => 'Ausgeloggt. Weiterleitung …',
 'No email match' => 'Es ist kein Benutzer mit dieser Email Adresse registriert',
 'Request pass' => 'Passwort anfordern',
 'Request pass legend' => 'Gib die Email Adresse ein, mit der Du dich registriert hast',
 'Request pass info' => 'Ein neues Passwort zusammen mit einem Aktivierungs-Link wird an diese Email Adresse geschickt.',
 'Not registered' => 'Noch nicht registriert?',
 'Login legend' => 'Gib deinen Benutzernamen und dein Passwort unten ein',
 'Remember me' => 'Automatisch bei jedem Besuch einloggen.',
 'Login info' => 'Wenn Du noch nicht registriert bist oder dein Passwort vergessen hast, klick auf den entsprechenden Link weiter unten.',
 'New password errors' => 'Fehler bei der Passwort-Anforderung',
 'New passworderrors info' => 'Der folgende Fehler muss korrigiert werden, bevor ein neues Passwort gesendet werden kann:',
 'Forget mail' => 'Eine Email mit Anweisungen, wie dein Passwort geändert werden kann, wurde an die angegebene Email Adresse geschickt. Solltest Du diese Email nicht erhalten, wende dich bitte an den Forums Administrator unter',
 'Email flood' => 'Unter diesem Account wurde während der letzten Stunde bereits eine Passwort-Anforderung geschickt. Bitte warte %s Minuten, bevor Du eine erneute Anforderung sendest.',
 'Login at dev1galaxy' => 'Login über dev1galaxy.org below',
 'OR Login via' => 'ODER Login via',
 'Login via git.devuan.org' => 'git.devuan.org'
);
?>
