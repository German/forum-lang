<?php
$lang_admin_censoring = array(
 'Must enter word message' => 'DU musst ein Wort eingeben, dass zensiert werden soll.',
 'Word updated redirect' => 'Zensiertes Wort aktualisiert. Weiterleitung …',
 'Word added redirect' => 'Zensiertes Wort hinzugefügt. Weiterleitung …',
 'Word removed redirect' => 'Zensiertes Wort entfernt. Weiterleitung …',
 'Censoring head' => 'Zensieren',
 'Add word subhead' => 'Wort hinzufügen',
 'Add word info' => 'Gib ein Wort ein, dass Du zensieren möchtest und dazu den passenden Ersatzbegriff. Platzhalter sind erlaubt (z.B. *netz* würde auf Firmennetz und auch auf Netzstrümpfe zutreffen). Zensierte Wörter gelten auch für Benutzernamen. Neue Benutzer können sich damit nicht unter einem Benutzernamen registrieren, der zensierte Wörter enthält. Die Suche berücksichtigt Groß/Kleinschreibung nicht.',
 'Censoring enabled' => '<strong>Zensieren ist aktviert in %s.</strong>',
 'Censoring disabled' => '<strong>Zensieren ist deaktiviert in %s.</strong>',
 'Censored word label' => 'Zensiertes Wort',
 'Replacement label' => 'Ersatz-Wort(e)',
 'Action label' => 'Aktion',
 'Edit remove subhead' => 'Ändere oder lösche Wörter',
 'No words in list' => 'Keine zensierten Wörter in der Liste.',
);
?>
