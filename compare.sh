#!/bin/bash
#
# Compare a language $1 to English

: ${COMPARE:=meld}

# Exit on ^C to child
trap "exit 1" 2

# A: needs to have same file sets
diff -y --suppress-common-lines -W 80 \
     <(find English -type f -printf "%P\n" | sort) \
     <(find $1      -type f -printf "%P\n" | sort) || exit 1

# B: check all map files
for Ef in $(find "English" -name '*.php' ) ; do
    Lf=$1/${Ef#English/}
    # B1: files are equal in keys
    if diff -y --suppress-common-lines -W 80 \
	    <(grep -oE "'.*=>" $Ef) \
	    <(grep -oE "'.*=>" $Lf) ; then
	# B2: review mappings with meld
	${COMPARE} $Ef $Lf
    else
	echo "$Ef and $Lf have different formats"
    fi
done
