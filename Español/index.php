<?php
$lang_index = array(
 'Topics' => 'Temas',
 'Link to' => 'Enlace a:',
 'Empty board' => 'Foros vacios.',
 'Newest user' => 'Ultimo usuario registrado: %s',
 'Users online' => 'Usuarios registrados conectados: %s',
 'Guests online' => 'Invitados conectados: %s',
 'No of users' => 'Número total de usuarios registrados: %s',
 'No of topics' => 'Número total de temas: %s',
 'No of posts' => 'Número total de mensajes: %s',
 'Online' => 'Conectados:',
 'Board info' => 'Información de los foros',
 'Board stats' => 'Estadisticas de los foros',
 'User info' => 'Información del usuario'
);
?>
