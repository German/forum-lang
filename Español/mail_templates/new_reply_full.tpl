Asunto: Respuesta a tema: <topic_subject>

<replier> ha respondido al tema <topic_subject> al que estás suscrito. Puede haber más respuestas nuevas, pero esta es la única notificación que recibirás hasta que vuelvas a visitar el foro de nuevo.

El mensaje se encuentra en <post_url>

El mensaje dice lo siguiente:
-----------------------------------------------------------------------

<message>

-----------------------------------------------------------------------

Puedes cancelar tu suscripción si vas a <unsubscribe_url>

--
<board_mailer> Mailer
(No respondas a este mensaje)
