Asunto: Cambio de dirección de correo electrónico solicitado

Hola <username>,

Has solicitado una nueva dirección de correo electrónico asignada a tu cuenta en el foro de discusión en <base_url>. Si no has solicitado este cambio o si ya no deseas cambiar tu dirección de correo electrónico, debes pasar por alto este mensaje. Sólo si visitas la página de activación de abajo, podrás cambiar tu dirección de correo. Para que la página de activación realice el trabajo, tienes que estar registrado en el foro.

Para cambiar tu correo electrónico, por favor visita la siguiente pagina:
<activation_url>

--
<board_mailer>
(No respondas a este mensaje)
