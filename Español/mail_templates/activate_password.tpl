Asunto: Nueva contraseña solicitada

Hola <username>,

Has solicitado una nueva contraseña asignada a tu cuenta en el foro de discusión en <base_url>. Si no has solicitado este cambio o si no deseas cambiar tu contraseña, debes ignorar este mensaje. Sólo si visitas la página de activación de abajo, podrás cambiar tu contraseña.

Tu nueva contraseña es: <new_password>

Para cambiar tu contraseña, por favor visita la siguiente página:
<activation_url>

--
<board_mailer>
(No respondas a este mensaje)
