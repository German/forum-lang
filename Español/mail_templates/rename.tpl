Asunto: Cuenta de usuario renombrada

Durante una actualización a los foros en <base_url> se determinó que tu nombre de usuario es demasiado similar a un usuario existente. En consecuencia, se ha modificado tu nombre de usuario.

Antiguo nombre de usuario: <old_username>
Nuevo nombre de usuario: <new_username>

Pedimos disculpas por las molestias causadas.

--
<board_mailer>
(No respondas a este mensaje)
