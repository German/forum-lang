<?php
$lang_mod_vsabr = array(
 'Robot title' => 'Are you human or robot?',
 'Robot question' => 'Please respond with an answer to the question (all lower case): <b>%s</b>',
 'Robot info' => 'Checking if this is requested by a real person and not an automated program.',
 'Robot test fail' => 'You answered incorrectly to the "Human or Robot" question, or you are a Bot!',
 'Complete the slogan: "software freedom ..."' => 'your way',
 'What is the codename of the current Devuan stable release?' => 'jessie',
 'What does the "U" in VUA stand for?' => 'unix',
 'What is the name of the Devuan unstable suite?' => 'ceres',
 'Devuan is a fork of what other distribution?' => 'debian',
 'What is the name of the Xfce file manager?' => 'thunar',
 'What is the last name of the creator of Linux?' => 'torvalds',
 'What is the default Desktop Environment for Devuan?' => 'xfce',
 'What are the first five prime numbers? (Do not use commas to separate the numbers)' => '2 3 5 7 11',
 'What is the name of the Devuan release coming after ASCII?' => 'beowulf',
 'What does SSD stand for?' => 'SSD',
);
?>
